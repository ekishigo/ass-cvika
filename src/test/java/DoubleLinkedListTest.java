import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.testng.Assert.*;

/**
 * Created by igorekishev on 21.02.2017.
 * Project cvi1_1
 */
public class DoubleLinkedListTest {

    private DoubleLinkedList<Integer> list;

    @Before
    public void setUp() throws Exception {
        list = new DoubleLinkedList<>();
    }

    @After
    public void tearDown() throws Exception {
        list = null;
    }

    @Test
    public void testAppend() {
        assertNull(list.getFirst());
        list.append(1);
        assertEquals(list.getFirst(), list.getLast());
        assertEquals(list.getFirst().value, (Integer) 1);
        for (int i = 0; i < 10; i++) {
            list.append(i);
            assertEquals(list.getLast().value, (Integer) i);
        }
    }

    @Test
    public void testPrepend() throws Exception {
        for (int i = 1; i <= 5; i++) {
            list.prepend(i);
        }
        int c = 5;
        for (Integer integer : list) {
            assertEquals(integer, (Integer) c);
            c--;
        }
    }

    @Test
    public void testIfSizeCorrectlyChangesAfterPrepend() {
        for (int i = 0; i < 10; i++) {
            list.prepend(i);
            assertTrue(list.size() == i + 1);
        }
    }

    @Test
    public void sizeCorrectlyChangesAfterAppend() {
        for (int i = 0; i < 10; i++) {
            list.append(i);
            assertTrue(list.size() == i + 1);
        }
    }

    @Test
    public void testCount() {
        List<Integer> arrayList = new ArrayList<>();
        List<Integer> linkedList = new LinkedList<>();
        for (int i = 0; i < 10; i++) {
            arrayList.add(i);
            linkedList.add(i);
        }
        assertTrue(10 == list.count(arrayList));
        assertTrue(10 == list.count(linkedList));
    }

    @Test
    public void testRemoveFront() {
        for (int i = 0; i < 10; i++) {
            list.append(i);
        }
        for (int i = 0; i < 10; i++) {
            Integer removed = list.removeFront();
            assertEquals(removed, (Integer) i);
        }
        assertTrue(0 == list.size());
    }

    @Test
    public void testRemoveBack() {
        for (int i = 0; i < 10; i++) {
            list.append(i);
        }
        for (int i = 9; i >= 0 ; i--) {
            Integer removed = list.removeBack();
            assertEquals(removed, (Integer) i);
        }
        assertTrue(list.size() == 0);
    }
}
