import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Iterator;
import java.util.LinkedList;

import static org.testng.Assert.*;

/**
 * Created by igorekishev on 22.02.2017.
 * Project cvi1_1
 */
public class ForwardIteratorTest {
    private Iterator<Integer> iterator;

    @BeforeMethod
    public void setUp() throws Exception {
        DoubleLinkedList<Integer> list = new DoubleLinkedList<>();
        for (int i = 1; i <= 5; i++) {
            list.append(i);
        }
        iterator = new ForwardIterator<>(list);
    }

    @AfterMethod
    public void tearDown() throws Exception {
        iterator = null;
    }

    @Test
    public void iteratesCorrectly() {
        for (int i = 1; i <= 5; i++) {
            assertEquals(iterator.next(), (Integer) i);
        }
    }

    @Test
    public void testHasNext() throws Exception {
        for (int i = 1; i < 5; i++) {
            assertTrue(iterator.hasNext());
            iterator.next();
        }
        assertFalse(iterator.hasNext());
    }
}