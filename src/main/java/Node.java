/**
 * Created by igorekishev on 21.02.2017.
 * Project cvi1_1
 */
class Node<E> {

    E value;
    Node<E> next;
    Node<E> prev;

    private Node(E value) {
        this.value = value;
    }

    static <E> Node<E> create(E element) {
        return new Node<>(element);
    }
}
