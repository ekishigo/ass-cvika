import com.google.common.base.Preconditions;

import java.util.Iterator;

/**
 * Created by igorekishev on 21.02.2017.
 * Project cvi1_1
 */
public class ForwardIterator<E> implements Iterator<E> {
    private Node<E> cursor;
    ForwardIterator(DoubleLinkedList<E> list) {
        Preconditions.checkNotNull(list);
        cursor = list.getFirst();
    }

    @Override
    public boolean hasNext() {
        return cursor != null && cursor.next != null;
    }

    @Override
    public E next() {
        E ret = cursor.value;
        cursor = cursor.next;
        return ret;
    }
}
