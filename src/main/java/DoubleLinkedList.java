import com.google.common.base.Preconditions;

import java.util.Collection;
import java.util.Iterator;

/**
 * Created by igorekishev on 21.02.2017.
 * Class represents generic double linked list. Elements of this collection can't be null.
 */
public class DoubleLinkedList<E> implements Iterable<E> {

    private Node<E> first;
    private Node<E> last;
    private int size;

    /**
     * Creates new empty double linked list
     */
    public DoubleLinkedList() {
        int size = 0;
    }

    /**
     * Appends element to the end of this collection and increments its size.
     *
     * @param element to be appended
     * @throws NullPointerException if specified element is NULL
     */
    public void append(E element) {
        Preconditions.checkNotNull(element);
        Node<E> newNode = Node.create(element);
        if (first == null) {
            first = newNode;
        } else {
            last.next = newNode;
            newNode.prev = last;
        }
        last = newNode;
        size++;
    }

    /**
     * Prepends element to the end of this collection and increments its size.
     *
     * @param element to be prepended.
     * @throws NullPointerException if specified element is NULL
     */
    public void prepend(E element) {
        Preconditions.checkNotNull(element);
        Node<E> newNode = Node.create(element);
        if (last == null) {
            last = newNode;
        } else {
            first.prev = newNode;
            newNode.next = first;
        }
        first = newNode;
        size++;
    }

    /**
     * Removes element from front of this list and decreases size.
     *
     * @return removed element or null if list is empty
     */
    public E removeFront() {
        if (size == 0) {
            return null;
        }
        E ret = first.value;
        first = first.next;
        size--;
        return ret;
    }


    /**
     * Removes element from front of this list and decreases size.
     *
     * @return removed element or null if list is empty
     */
    public E removeBack() {
        if (size == 0) {
            return null;
        }
        E ret = last.value;
        last = last.prev;
        size--;
        return ret;
    }

    /**
     * Appends specified collection to the end of this collection.
     *
     * @param source collection to be appended
     * @return true if this collection was changed after appending, false otherwise
     * @throws NullPointerException if one or more elements of the specified collection is NULL
     */
    public boolean appendAll(Collection<? extends E> source) {
        if (source == null) {
            return false;
        }

        DoubleLinkedList<E> otherCollection = new DoubleLinkedList<>();
        source.forEach(otherCollection::append);

        Node<E> otherFirst = otherCollection.getFirst();
        Node<E> otherLast = otherCollection.getLast();
        if (isEmpty()) {
            this.first = otherFirst;
        } else {
            this.last.next = otherFirst;
            otherFirst.prev = last;
        }
        this.last = otherLast;

        return true;
    }

    /**
     * Takes all elements of this list and add them to specified collection.
     *
     * @param destination destination collection
     */
    public void takeAll(Collection<? super E> destination) {
        Preconditions.checkNotNull(destination);
        this.forEach(destination::add);
    }

    /**
     * Returns new ForwardIterator implementation of java.util.Iterator wich iterates collection forward.
     *
     * @return Iterator
     */
    public Iterator<E> forward() {
        return new ForwardIterator<>(this);
    }

    /**
     * Returns new ForwardIterator implementation of java.util.Iterator wich iterates collection forward.
     *
     * @return Iterator
     */
    public Iterator<E> backward() {
        return new ForwardIterator<>(this);
    }

    /**
     * Returns true if this collection is empty, false otherwise
     *
     * @return boolean
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns size of the specified collection
     *
     * @param collection which size you want to determine
     * @return size of the collection
     */
    public int count(Collection<?> collection) {
        return collection.size();
    }

    /**
     * Returns size of this collection
     *
     * @return size
     */
    public int size() {
        return size;
    }

    public Iterator<E> iterator() {
        return forward();
    }

    protected Node<E> getFirst() {
        return first;
    }

    protected Node<E> getLast() {
        return last;
    }
}
