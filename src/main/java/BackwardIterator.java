import com.google.common.base.Preconditions;

import java.util.Iterator;

/**
 * Created by igorekishev on 21.02.2017.
 * Project cvi1_1
 */
public class BackwardIterator<E> implements Iterator<E> {
    private Node<E> cursor;
    BackwardIterator(DoubleLinkedList<E> list) {
        Preconditions.checkNotNull(list);
        cursor = list.getLast();
    }

    @Override
    public boolean hasNext() {
        return cursor != null && cursor.prev != null;
    }

    @Override
    public E next() {
        E ret = cursor.value;
        cursor = cursor.prev;
        return ret;
    }

}
